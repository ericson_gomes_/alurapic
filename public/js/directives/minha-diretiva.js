angular.module('minhaDiretiva',[])
.directive('meuPainel',function(){

	var ddo ={};

	/*define como a diretiva vai ser utilizada 
		A = Atributo
		E = Element
	*/
	ddo.restrict = "AE";

	ddo.transclude = true;

	ddo.scope ={
		titulo: '@',
	};

	ddo.templateUrl = 'js/directives/meu-painel.html'

	/* forma de incluir template concatenando*/
	/*ddo.template = 	'<div class="panel-heading">'
						+'<h3 class="panel-title">{{titulo}}</h3>'+
	                +'</div>'
					+'<div class="panel-body">'
	                +'</div>';*/


	return ddo;


}).directive('minhaFoto',function(){
	var ddo = {};

	ddo.restrict = "AE";

	ddo.scope = {
		titulo: '@',
		url: '@',
	};

	ddo.templateUrl = 'js/directives/minha-foto.html';

	return ddo;
});