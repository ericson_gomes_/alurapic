angular.module('alurapic',['minhaDiretiva','ngAnimate','ngRoute'])
	.config(function($routeProvider,$locationProvider){

		$locationProvider.html5Mode(true);

		$routeProvider.when('/fotos',{
			templateUrl: 'partials/fotos.html',
			controller: 'FotosController'
		});

		$routeProvider.when('/fotos/new',{
			templateUrl: 'partials/foto-new.html',
		});

		$routeProvider.otherwise({redirectTo:'/fotos'});

	});